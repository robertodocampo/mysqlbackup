# MysqlBackup Docker Container

This is a very simple aproach to the container technology



## This application consists of:

*   Simple Script That Backups a Mysql BBDD and Optionally sends the Zipped Dump by FTP
*   The database will be backed up once a day


## Author

*   Roberto Docampo Suarez <informatica@robertodocampo.com>

## How to

*   [Build The Image]
        Simply run the ['''docker build -t robertodocampo/mysqlbackup . '''] command on the Downloaded Repository
*   [Set The Database/Server to Backup]
	Set the next Environment Variables on your docker run command
        MYSQL_USER    : The user authorized to dump the database
        MYSQL_PASS    : The pass for this user (optional) 
        MYSQL_SERVER  : The IP of the Mysql Server
        MYSQL_DB      : The DataBase Name

*   [How Can I do to retrieve the dumped file ]
	The Scripts stores the File locally (see next section) or (optional) can send it by FTP (see FTP Section)

*   [Storing the dump locally]
        The "/data" folder of the container has the output of the dump. You can bin it to a folder on your docker anfitrion
	or asociate it to a docker data volume

*   [Send the dump by FTP]
	Set the next Environment Variables on your docker run command
	FTP_USER      : Username 
        FTP_PASS      : Pass for the Username
        FTP_ADDRESS   : The FTP address. ftp://servername:port/destination_folder


*   [Run the Container]
        docker run --rm -ti --env MYSQL_USER=user --env MYSQL_PASS=pass --env MYSQL_SERVER=localhost --env MYSQL_DB=mydatabase \
                            --env FTP_USER=user --env FTP_PASS=pass --env FTP_ADDRESS=ftp://localhost/mysqldirectory

	



