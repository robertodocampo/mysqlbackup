#!/bin/bash
###############################################################################
#   Utility to dump a Mysql Database
#   Author: Roberto Docampo Suarez (Groves) <informatica@robertodocampo.com>
#
#   Free to Distribute
###############################################################################

TMP=/data
OUT=$MYSQL_DB.$(date +%Y%m%d).sql
LOG=/var/log/mysqlbackup.log

# Delete possible pre-execution
#rm /$TMP/*.sql 2>/dev/null
#rm /$TMP/*.gz  2>/dev/null
# 20170117 -- Deleted for security pourposes

echo "Starting Backup Process ....$(date)" >>$LOG

if [ -v "$MYSQL_PASS" ]; then
    PASS="--password $MYSQL_PASS"
    exit 1
fi  



# Dump the Database to file
mysqldump --user $MYSQL_USER --single-transaction \
          --add-drop-table --host $MYSQL_SERVER $PASS \
          $MYSQL_DB > $TMP\/$OUT 2>> $LOG
echo "Backup finished on " $OUT >>$LOG

# Compress the sql image to best efficiency
gzip $TMP\/$OUT >> $LOG 2>&1

echo "Backup Zipped" >>$LOG


# FTP Output
if [ -v "$FTP_USER" ]; then
    exit 
fi  


curl -v -T $TMP\/$OUT.gz $FTP_ADDRESS\/$OUT.gz --user $FTP_USER:$FTP_PASS >>$LOG 2>&1

echo "End Of Backup $(date)" >> $LOG
echo "     " >>$LOG


